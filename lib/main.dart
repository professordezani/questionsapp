// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors

import 'package:flutter/material.dart';

void main() {
  runApp(QuestionsApp());
}

class QuestionsApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: QuestionPage(),
    );
  }
}

class QuestionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("Q&A"),
            Text("CODE: CODIGO"),
          ],
        ),
      ),
      body: Container(
        margin: EdgeInsets.all(10),
        child: ListView(
          children: [
            Card(
              // elevation: 4,
              child: Container(
                margin: const EdgeInsets.all(10),
                child: Column(
                  children: [
                    TextField(
                      maxLines: 8,
                      minLines: 4,
                      decoration: InputDecoration(
                        hintText: "Faça sua pergunta...",
                        border: InputBorder.none,
                      ),
                      autofocus: true,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Enviar como anônimo",
                            style: TextStyle(fontSize: 12, color: Colors.grey)),
                        ElevatedButton(
                          onPressed: () {},
                          child: Text("Enviar"),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Card(
              // elevation: 6,
              child: ListTile(
                contentPadding: EdgeInsets.fromLTRB(16, 4, 16, 10),
                leading: CircleAvatar(child: Text("A")),
                title: Text("Anonymous"),
                subtitle: Text(
                    "Pergunta muito grande para caber numa única linha, sendo assim não conseguiria visuazá-la por inteiro. Tem como resolver esse tipo de aplicação?Pergunta muito grande para caber numa única linha, sendo assim não conseguiria visuazá-la por inteiro. Tem como resolver esse tipo de aplicação?Pergunta muito grande para caber numa única linha, sendo assim não conseguiria visuazá-la por inteiro. Tem como resolver esse tipo de aplicação?"),
                trailing: Container(
                  width: 60,
                  child: Row(
                    children: [
                      IconButton(
                        onPressed: null,
                        icon: Icon(Icons.thumb_up),
                      ),
                      Text('12'),
                    ],
                  ),
                ),
              ),
            ),
            Card(
              child: ListTile(
                contentPadding: EdgeInsets.fromLTRB(16, 4, 16, 10),
                leading: CircleAvatar(child: Text("A")),
                title: Text("Anonymous"),
                subtitle: Text(
                    "Pergunta muito grande para caber numa única linha, sendo assim não conseguiria visuazá-la por inteiro. Tem como resolver esse tipo de aplicação?Pergunta muito grande para caber numa única linha, sendo assim não conseguiria visuazá-la por inteiro. Tem como resolver esse tipo de aplicação?Pergunta muito grande para caber numa única linha, sendo assim não conseguiria visuazá-la por inteiro. Tem como resolver esse tipo de aplicação?"),
                trailing: Container(
                  width: 60,
                  child: Row(
                    children: [
                      IconButton(
                        onPressed: null,
                        icon: Icon(Icons.thumb_up),
                      ),
                      Text('12'),
                    ],
                  ),
                ),
              ),
            ),
            Card(
              child: ListTile(
                contentPadding: EdgeInsets.fromLTRB(16, 4, 16, 10),
                leading: CircleAvatar(child: Text("A")),
                title: Text("Anonymous"),
                subtitle: Text(
                    "Pergunta muito grande para caber numa única linha, sendo assim não conseguiria visuazá-la por inteiro. Tem como resolver esse tipo de aplicação?Pergunta muito grande para caber numa única linha, sendo assim não conseguiria visuazá-la por inteiro. Tem como resolver esse tipo de aplicação?Pergunta muito grande para caber numa única linha, sendo assim não conseguiria visuazá-la por inteiro. Tem como resolver esse tipo de aplicação?"),
                trailing: Container(
                  width: 60,
                  child: Row(
                    children: [
                      IconButton(
                        onPressed: null,
                        icon: Icon(Icons.thumb_up),
                      ),
                      Text('12'),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
